import { Component } from '@angular/core';
import {NbMenuItem, NbSidebarService} from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ticket-reservation-frontend';
  constructor(private readonly sidebarService: NbSidebarService) {
  }
  items: NbMenuItem[] = [
    {
      title: 'Home',
      icon: 'home-outline',
      link: '/home',
      home: true
    },
    {
      title: 'Movies',
      icon: 'people-outline',
      link: '/movie'
    }
  ];
  toggleSidebar(): boolean {
    this.sidebarService.toggle();
    return false;
  }
}
