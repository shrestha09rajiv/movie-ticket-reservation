import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SidebarComponent} from './component/sidebar/sidebar.component';
import {MovieComponent} from './component/movie/movie.component';
import {MovieFormComponent} from './component/movie/movie-form/movie-form.component';

const routes: Routes = [
  {
    path: 'home',
    component: SidebarComponent
  },
  {
    path: 'movie',
    component: MovieComponent
  },
  {
    path: 'add/movie',
    component: MovieFormComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
