import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {

  constructor( private router: Router) { }

  ngOnInit(): void {
  }

  addMovie() {
    this.router.navigate(['add/movie']);
  }
}
