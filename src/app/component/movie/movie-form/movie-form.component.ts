import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-movie-form',
  templateUrl: './movie-form.component.html',
  styleUrls: ['./movie-form.component.css']
})
export class MovieFormComponent implements OnInit {

  movieForm = new FormGroup({
    movieName: new FormControl('', Validators.required),
    genre: new FormControl('', Validators.required),
    releaseDate: new FormControl('', Validators.required),
  });

  constructor(private formBuilder: FormBuilder) {
  }

  get movieName() {
    return this.movieForm.get('movieName');
  }

  get genre() {
    return this.movieForm.get('genre');
  }

  get releaseDate() {
    return this.movieForm.get('releaseDate');
  }

  ngOnInit(): void {
    console.log('hello');
  }

  onSubmit() {
    console.log(this.movieForm);
  }

}
